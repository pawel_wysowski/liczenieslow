﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class Program
    {
        static void Main(string[] args)
        {

            try
            {
                char car;
                int count = 0;
                int words = 0;

                string path = System.Reflection.Assembly.GetExecutingAssembly().Location;
                var directory = Path.GetDirectoryName(path);
                using (StreamReader bs = new StreamReader(new FileStream(directory+@"\test2.txt", FileMode.Open)))
                {


                    while (bs.Peek() >= 0)
                    {
                        
                        car = (char)bs.Read();
                        if (car == 32 || car == 9 || car == '\r' || car == '\n')
                        {
                            if (count>=1)
                            words++;
                            count = 0;
                        }
                        else count++;


                    }

                    if (count >= 1)
                        words++;
                }
                Console.WriteLine(words);
            }
            catch (Exception e)
            {
                Console.WriteLine("The file could not be read:");
                Console.WriteLine(e.Message);
            }

            Console.ReadKey();


    }
    }
}
